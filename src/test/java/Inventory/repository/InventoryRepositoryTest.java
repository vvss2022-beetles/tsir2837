package inventory.repository;

import com.sun.javafx.collections.ObservableListWrapper;
import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class InventoryRepositoryTest {
    @Mock
    Inventory inventory;
    @InjectMocks
    InventoryRepository inventoryRepository;

    @Test
    public void getAutoPartId_success() {
        Mockito.when(inventory.getAutoPartId()).thenReturn(0);

        int result = inventoryRepository.getAutoPartId();

        Assertions.assertEquals(0, result);
        Mockito.verify(inventory).getAutoPartId();
    }

    @Test
    public void getAutoPartId_failure() {
        Mockito.when(inventory.getAutoPartId()).thenThrow(new RuntimeException());

        Assertions.assertThrows(RuntimeException.class, () -> inventoryRepository.getAutoPartId());

        Mockito.verify(inventory).getAutoPartId();
    }

    @Test
    public void getAutoProductId_success() {
        Mockito.when(inventory.getAutoProductId()).thenReturn(0);

        int result = inventoryRepository.getAutoProductId();

        Assertions.assertEquals(0, result);
        Mockito.verify(inventory).getAutoProductId();
    }

    @Test
    public void getAutoProductId_failure() {
        Mockito.when(inventory.getAutoProductId()).thenThrow(new RuntimeException());

        Assertions.assertThrows(RuntimeException.class, () -> inventoryRepository.getAutoProductId());

        Mockito.verify(inventory).getAutoProductId();
    }

    @Test
    public void getAllProducts_success() {
        ObservableList<Product> products = getProducts();
        Mockito.when(inventory.getProducts()).thenReturn(products);

        ObservableList<Product> result = inventoryRepository.getAllProducts();

        Assertions.assertEquals(products, result);
        Mockito.verify(inventory).getProducts();
    }

    @Test
    public void getAllProducts_failure() {
        Mockito.when(inventory.getProducts()).thenThrow(new RuntimeException());

        Assertions.assertThrows(RuntimeException.class, () -> inventoryRepository.getAllProducts());

        Mockito.verify(inventory).getProducts();
    }

    @Test
    public void getAllParts_success() {
        ObservableList<Part> parts = getParts();
        Mockito.when(inventory.getAllParts()).thenReturn(parts);

        ObservableList<Part> result = inventoryRepository.getAllParts();

        Assertions.assertEquals(parts, result);
        Mockito.verify(inventory).getAllParts();
    }

    @Test
    public void getAllParts_failure() {
        Mockito.when(inventory.getAllParts()).thenThrow(new RuntimeException());

        Assertions.assertThrows(RuntimeException.class, () -> inventoryRepository.getAllParts());

        Mockito.verify(inventory).getAllParts();
    }

    private ObservableList<Product> getProducts() {
        ObservableList<Product> products = new ObservableListWrapper<>(new ArrayList<>());
        Product product1 = new Product(1, "name1", 1, 5, 1, 20, new ObservableListWrapper<>(new ArrayList<>()));
        Product product2 = new Product(2, "name2", 2, 6, 2, 21, new ObservableListWrapper<>(new ArrayList<>()));

        products.add(product1);
        products.add(product2);
        return products;
    }

    private ObservableList<Part> getParts() {
        ObservableList<Part> parts = new ObservableListWrapper<>(new ArrayList<>());
        Part part1 = new InhousePart(1, "name1", 1, 5, 1, 20, 0);
        Part part2 = new InhousePart(2, "name2", 2, 6, 2, 21, 1);

        parts.add(part1);
        parts.add(part2);
        return parts;
    }
}
