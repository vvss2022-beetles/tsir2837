package inventory.repository;

import com.sun.javafx.collections.ObservableListWrapper;
import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.service.InventoryService;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class InventoryRepositoryIntegrationTest {
    private static final String NAME_VALUE = "name2";
    private static final double PRICE_VALUE = 2.0;
    private static final int MIN_VALUE = 2;
    private static final int MAX_VALUE = 21;
    private static final int IN_STOCK_VALUE = 6;
    private static final ObservableList<Part> parts = new ObservableListWrapper<>(List.of(new InhousePart(1, "", 0, 1, 1, 1, 1), new InhousePart(2, "", 0, 1, 1, 1, 1)));
    private static final Product PRODUCT_VALUE = new Product(0, NAME_VALUE, PRICE_VALUE, IN_STOCK_VALUE, MIN_VALUE, MAX_VALUE, parts);
    @Mock
    private Product product;
    private static InventoryRepository repository = new InventoryRepository();
    private static InventoryService service;

    @BeforeAll
    public static void setup() {
        service = new InventoryService(repository);
    }

    @Test
    @Order(1)
    public void addProduct() {
        lenient().when(product.equalTo(PRODUCT_VALUE)).thenReturn(true);
        int originalSize = repository.getInventory().getProducts().size();

        Assertions.assertEquals(originalSize, service.getAllProducts().size());
        
        service.addProduct(NAME_VALUE, PRICE_VALUE, IN_STOCK_VALUE, MIN_VALUE, MAX_VALUE, parts);
        int resultSize = repository.getInventory().getProducts().size();

        Assertions.assertEquals(originalSize + 1, resultSize);
        Assertions.assertEquals(resultSize, service.getAllProducts().size());
    }

    @Test
    @Order(2)
    public void deleteProduct() {
        lenient().when(product.equalTo(PRODUCT_VALUE)).thenReturn(true);
        service.addProduct(NAME_VALUE, PRICE_VALUE, IN_STOCK_VALUE, MIN_VALUE, MAX_VALUE, parts);
        int originalSize = repository.getInventory().getProducts().size();
        Assertions.assertEquals(originalSize, service.getAllProducts().size());
        Assertions.assertTrue(product.equalTo(service.getAllProducts().get(0)));

        service.deleteProduct(PRODUCT_VALUE);
        int resultSize = repository.getInventory().getProducts().size();

        Assertions.assertEquals(originalSize - 1, resultSize);
        Assertions.assertEquals(resultSize, service.getAllProducts().size());
        Assertions.assertFalse(service.getAllProducts().contains(product));
    }
}
