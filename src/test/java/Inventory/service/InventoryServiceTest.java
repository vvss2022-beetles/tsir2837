package inventory.service;

import com.sun.javafx.collections.ObservableListWrapper;
import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;


@ExtendWith(MockitoExtension.class)
public class InventoryServiceTest {
    @Mock
    InventoryRepository inventoryRepository;
    @InjectMocks
    InventoryService inventoryService;

    @Test
    public void getAllProducts_success() {
        ObservableList<Product> products = getProducts();
        Mockito.when(inventoryRepository.getAllProducts()).thenReturn(products);

        ObservableList<Product> result = inventoryService.getAllProducts();

        Assertions.assertEquals(products, result);
        Mockito.verify(inventoryRepository).getAllProducts();
    }

    @Test
    public void getAllProducts_failure() {
        Mockito.when(inventoryRepository.getAllProducts()).thenThrow(new RuntimeException());

        Assertions.assertThrows(RuntimeException.class, () -> inventoryService.getAllProducts());

        Mockito.verify(inventoryRepository).getAllProducts();
    }

    @Test
    public void getAllParts_success() {
        ObservableList<Part> parts = getParts();
        Mockito.when(inventoryRepository.getAllParts()).thenReturn(parts);

        ObservableList<Part> result = inventoryService.getAllParts();

        Assertions.assertEquals(parts, result);
        Mockito.verify(inventoryRepository).getAllParts();
    }

    @Test
    public void getAllParts_failure() {
        Mockito.when(inventoryRepository.getAllParts()).thenThrow(new RuntimeException());

        Assertions.assertThrows(RuntimeException.class, () -> inventoryService.getAllParts());

        Mockito.verify(inventoryRepository).getAllParts();
    }

    @Test
    public void lookupPart_success() {
        Part part = getPart();
        Mockito.when(inventoryRepository.lookupPart("Test")).thenReturn(part);

        Part result = inventoryService.lookupPart("Test");

        Assertions.assertEquals(part, result);
        Mockito.verify(inventoryRepository).lookupPart("Test");
    }

    @Test
    public void lookupPart_failure() {
        Mockito.when(inventoryRepository.lookupPart("Test")).thenThrow(new RuntimeException());

        Assertions.assertThrows(RuntimeException.class, () -> inventoryService.lookupPart("Test"));

        Mockito.verify(inventoryRepository).lookupPart("Test");
    }

    @Test
    public void lookupProduct_success() {
        Product product = getProduct();
        Mockito.when(inventoryRepository.lookupProduct("Test")).thenReturn(product);

        Product result = inventoryService.lookupProduct("Test");

        Assertions.assertEquals(product, result);
        Mockito.verify(inventoryRepository).lookupProduct("Test");
    }

    @Test
    public void lookupProduct_failure() {
        Mockito.when(inventoryRepository.lookupProduct("Test")).thenThrow(new RuntimeException());

        Assertions.assertThrows(RuntimeException.class, () -> inventoryService.lookupProduct("Test"));

        Mockito.verify(inventoryRepository).lookupProduct("Test");
    }

    @Test
    public void deletePart_success() {
        Part part = getPart();

        inventoryService.deletePart(part);

        Mockito.verify(inventoryRepository).deletePart(part);
    }

    @Test
    public void deletePart_failure() {
        Part part = getPart();
        Mockito.doThrow(new RuntimeException()).when(inventoryRepository).deletePart(part);
        Assertions.assertThrows(RuntimeException.class, () -> inventoryService.deletePart(part));

        Mockito.verify(inventoryRepository).deletePart(part);
    }

    private ObservableList<Product> getProducts() {
        ObservableList<Product> products = new ObservableListWrapper<>(new ArrayList<>());
        Product product1 = new Product(1, "name1", 1, 5, 1, 20, new ObservableListWrapper<>(new ArrayList<>()));
        Product product2 = new Product(2, "name2", 2, 6, 2, 21, new ObservableListWrapper<>(new ArrayList<>()));

        products.add(product1);
        products.add(product2);
        return products;
    }

    private ObservableList<Part> getParts() {
        ObservableList<Part> parts = new ObservableListWrapper<>(new ArrayList<>());
        Part part1 = new InhousePart(1, "name1", 1, 5, 1, 20, 0);
        Part part2 = new InhousePart(2, "name2", 2, 6, 2, 21, 1);

        parts.add(part1);
        parts.add(part2);
        return parts;
    }

    private Part getPart() {
        return new InhousePart(1, "name1", 1, 5, 1, 20, 0);
    }

    private Product getProduct() {
        return new Product(1, "name1", 1, 5, 1, 20, new ObservableListWrapper<>(new ArrayList<>()));
    }
}
