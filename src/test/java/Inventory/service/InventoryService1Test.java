package inventory.service;


import com.sun.javafx.collections.ObservableListWrapper;
import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.OS;

import java.util.List;

@DisplayName("Inventory Service Test")
public class InventoryService1Test {

    private static final String NAME_VALUE = "Nume Produs";
    private static final String ERROR_MESSAGE_INVALID_MIN = "The inventory level must be greater than 0. ";
    private static final String ERROR_MESSAGE_INVALID_PRICE = "The price must be greater than $0. ";
    private static final String ERROR_MESSAGE_INVALID_MIN_PRICE = "The inventory level must be greater than 0. The price must be greater than $0. ";
    private static final int MAX_VALUE = Integer.MAX_VALUE;
    private static final int IN_STOCK_VALUE = 20;
    private static InventoryService service;
    private final ObservableList parts = new ObservableListWrapper(List.of(new InhousePart(1, "", 0, 1, 1, 1, 1), new InhousePart(2, "", 0, 1, 1, 1, 1)));

    @BeforeAll
    public static void setup() {
        service = new InventoryService(new InventoryRepository());
    }

    @Test
    @Tag("invalid")
    @Order(1)
    public void addProduct_invalidMinParameter_fail() {
        //arrange
        int min = -1;
        double price = 0.01;

        //act
        String result = service.addProduct(NAME_VALUE, price, IN_STOCK_VALUE, min, MAX_VALUE, (ObservableList<Part>) parts);

        //assert
        Assertions.assertEquals(ERROR_MESSAGE_INVALID_MIN, result);
    }

    @Test
    @Tag("invalid")
    @Order(5)
    public void addProduct_invalidPriceParameter_fail() {
        //arrange
        int min = 1;
        double price = 0;

        //act
        String result = service.addProduct(NAME_VALUE, price, IN_STOCK_VALUE, min, MAX_VALUE, (ObservableList<Part>) parts);

        //assert
        Assertions.assertEquals(ERROR_MESSAGE_INVALID_PRICE, result);
    }

    @Test
    @Tag("valid")
    @Order(4)
    @DisabledOnOs(OS.MAC)
    public void addProduct_validParameters_success() {
        //arrange
        int min = 1;
        double price = 0.01;

        //act
        String result = service.addProduct(NAME_VALUE, price, IN_STOCK_VALUE, min, MAX_VALUE, (ObservableList<Part>) parts);

        //assert
        Assertions.assertEquals("", result);

    }

    @Test
    @Tag("invalid")
    @Order(3)
    @Timeout(500)
    public void addProduct_invalidMinPriceParameter_fail() {
        //arrange
        int min = -1;
        double price = 0;

        //act
        String result = service.addProduct(NAME_VALUE, price, IN_STOCK_VALUE, min, MAX_VALUE, (ObservableList<Part>) parts);

        //assert
        Assertions.assertEquals(ERROR_MESSAGE_INVALID_MIN_PRICE, result);
    }

    @Test
    @Tag("valid")
    @Order(2)
    public void addProduct_withMaxValueOfMinParameter_success() {
        //arrange
        int min = Integer.MAX_VALUE;
        double price = 0.01;

        //act
        String result = service.addProduct(NAME_VALUE, price, Integer.MAX_VALUE, min, MAX_VALUE, (ObservableList<Part>) parts);

        //assert
        Assertions.assertEquals("", result);
    }


}
