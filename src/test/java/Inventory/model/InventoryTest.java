package inventory.model;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.List;

public class InventoryTest {
    private Inventory inventory = new Inventory();
    private static Product PRODUCT_VALUE_1 = new Product(1, "name1", 1, 5, 1, 20, new ObservableListWrapper<>(new ArrayList<>()));
    private static Product PRODUCT_VALUE_2 = new Product(2, "name2", 2, 6, 2, 21, new ObservableListWrapper<>(new ArrayList<>()));

    @Test
    public void lookupProduct_nullSearchItem_fail() {
        inventory.setProducts(getProducts());

        Assertions.assertThrows(NullPointerException.class, () -> inventory.lookupProduct(null));
    }

    @Test
    public void lookupProduct_withoutProducts_success() {
        inventory.setProducts(new ObservableListWrapper<>(new ArrayList<>()));

        Product p = inventory.lookupProduct("ceva");

        Assertions.assertNull(p);
    }

    @Test
    public void lookupProduct_withProductsAndNotFoundProduct_success() {
        inventory.setProducts(getProducts());

        Product p = inventory.lookupProduct("ceva");

        Assertions.assertNull(p);
    }

    @Test
    public void lookupProduct_withProductsAndFoundProduct_success() {
        inventory.setProducts(getProducts());

        Product p = inventory.lookupProduct("1");

        Assertions.assertEquals(PRODUCT_VALUE_1, p);
    }

    private ObservableList<Product> getProducts() {
        ObservableList<Product> products = new ObservableListWrapper<>(new ArrayList<>());
        products.add(PRODUCT_VALUE_1);
        products.add(PRODUCT_VALUE_2);
        return products;
    }
}
