package inventory.model;

import com.sun.javafx.collections.ObservableListWrapper;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ProductIntegrationTest {
    private static final String NAME_VALUE = "name2";
    private static final double PRICE_VALUE = 2.0;
    private static final int MIN_VALUE = 2;
    private static final int MAX_VALUE = 21;
    private static final int IN_STOCK_VALUE = 6;
    private final ObservableList<Part> parts = new ObservableListWrapper<>(List.of(new InhousePart(1, "", 0, 1, 1, 1, 1), new InhousePart(2, "", 0, 1, 1, 1, 1)));

    private Product product = new Product(0, NAME_VALUE, PRICE_VALUE, IN_STOCK_VALUE, MIN_VALUE, MAX_VALUE, parts);
    private static InventoryRepository repository = new InventoryRepository();
    private static InventoryService service;

    @BeforeAll
    public static void setup() {
        service = new InventoryService(repository);
    }

    @Test
    @Order(1)
    public void addProduct() {
        int originalSize = repository.getInventory().getProducts().size();

        Assertions.assertEquals(originalSize, service.getAllProducts().size());

        Assertions.assertFalse(service.getAllProducts().contains(product));

        service.addProduct(NAME_VALUE, PRICE_VALUE, IN_STOCK_VALUE, MIN_VALUE, MAX_VALUE, parts);
        int resultSize = repository.getInventory().getProducts().size();

        Assertions.assertEquals(originalSize + 1, resultSize);
        Assertions.assertEquals(resultSize, service.getAllProducts().size());
        Assertions.assertTrue(service.getAllProducts().contains(product));
    }

    @Test
    @Order(2)
    public void deleteProduct() {
        service.addProduct(NAME_VALUE, PRICE_VALUE, IN_STOCK_VALUE, MIN_VALUE, MAX_VALUE, parts);
        int originalSize = repository.getInventory().getProducts().size();

        Assertions.assertEquals(originalSize, service.getAllProducts().size());
        Assertions.assertTrue(service.getAllProducts().contains(product));

        service.deleteProduct(product);
        int resultSize = repository.getInventory().getProducts().size();

        Assertions.assertEquals(originalSize - 1, resultSize);
        Assertions.assertEquals(resultSize, service.getAllProducts().size());
        Assertions.assertFalse(service.getAllProducts().contains(product));
    }

}
