package inventory.model;

import com.sun.javafx.collections.ObservableListWrapper;
import inventory.service.InventoryService;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ProductTest {
    private static final String ERROR_MESSAGE_INVALID_MIN = "The inventory level must be greater than 0. ";
    private static final String NAME_VALUE = "name2";
    private static final double PRICE_VALUE = 2.0;
    private static final int MIN_VALUE = 2;
    private static final int MAX_VALUE = 21;
    private static final int IN_STOCK_VALUE = 6;
    private final ObservableList<Part> parts = new ObservableListWrapper<>(List.of(new InhousePart(1, "", 0, 1, 1, 1, 1), new InhousePart(2, "", 0, 1, 1, 1, 1)));

    private Product product = new Product(0, NAME_VALUE, PRICE_VALUE, IN_STOCK_VALUE, MIN_VALUE, MAX_VALUE, parts);

    @Test
    public void toString_success() {
        String result = product.toString();
        Assertions.assertEquals("P,0,name2,2.0,6,2,21", result);
    }

    @Test
    void isValidProduct_success_invalid() {
        int min = -1;
        //act
        String result = Product.isValidProduct(NAME_VALUE, PRICE_VALUE, IN_STOCK_VALUE, min, MAX_VALUE, parts, "");
        //assert
        Assertions.assertEquals(ERROR_MESSAGE_INVALID_MIN, result);
    }

    @Test
    void isValidProduct_success_valid() {
        int min = 2;
        //act
        String result = Product.isValidProduct(NAME_VALUE, PRICE_VALUE, IN_STOCK_VALUE, min, MAX_VALUE, parts, "");
        //assert
        Assertions.assertEquals("", result);
    }

    @Test
    void getMax_success(){
        int max = product.getMax();
        Assertions.assertEquals(MAX_VALUE, max);
    }

    @Test
    void getMin_success(){
        int min = product.getMin();
        Assertions.assertEquals(MIN_VALUE, min);
    }
}
