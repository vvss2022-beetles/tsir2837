package inventory.controller;

import inventory.model.Part;
import inventory.model.Product;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class AddProductController implements Initializable, Controller {

    // Declare fields
    private Stage stage;
    private Parent scene;
    private ObservableList<Part> addedParts = FXCollections.observableArrayList();
    private String errorMessage = new String();

    private InventoryService service;

    @FXML
    private TextField minTxt;

    @FXML
    private TextField maxTxt;

    @FXML
    private TextField productIdTxt;

    @FXML
    private TextField nameTxt;

    @FXML
    private TextField inventoryTxt;

    @FXML
    private TextField priceTxt;

    @FXML
    private TextField productSearchTxt;

    @FXML
    private TableView<Part> unaddedPartsTableView;

    @FXML
    private TableColumn<Part, Integer> unaddedPartsIdCol;

    @FXML
    private TableColumn<Part, String> unaddedPartsNameCol;

    @FXML
    private TableColumn<Part, Double> unaddedPartsInventoryCol;

    @FXML
    private TableColumn<Part, Integer> unaddedPartsPriceCol;

    @FXML
    private TableView<Part> addedPartsTableView;

    @FXML
    private TableColumn<Part, Integer> addedPartsIdCol;

    @FXML
    private TableColumn<Part, String> addedPartsNameCol;

    @FXML
    private TableColumn<Part, Double> addedPartsInventoryCol;

    @FXML
    private TableColumn<Part, Integer> addedPartsPriceCol;

    public AddProductController() {
    }

    public void setService(InventoryService service) {
        this.service = service;
        unaddedPartsTableView.setItems(service.getAllParts());

    }

    /**
     * Initializes the controller class and populates table view.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Populate add product table view
        unaddedPartsIdCol.setCellValueFactory(new PropertyValueFactory<>("partId"));
        unaddedPartsNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        unaddedPartsInventoryCol.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        unaddedPartsPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
    }

    /**
     * Method to add to button handler to switch to scene passed as source
     *
     * @param event
     * @param source
     * @throws IOException
     */
    @FXML
    private void displayScene(ActionEvent event, String source) throws IOException {
        stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(source));
        //scene = FXMLLoader.load(getClass().getResource(source));
        scene = loader.load();
        Controller ctrl = loader.getController();
        ctrl.setService(service);
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * Method to add values of addParts to the bottom table view of the scene.
     */
    public void updateAddedPartsTableView() {
        addedPartsTableView.setItems(addedParts);

        addedPartsIdCol.setCellValueFactory(new PropertyValueFactory<>("partId"));
        addedPartsNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        addedPartsInventoryCol.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        addedPartsPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
    }

    /**
     * Ask user for confirmation before deleting selected part from current product.
     *
     * @param event
     */
    @FXML
    void handleDeletePartProduct(ActionEvent event) {
        Part part = addedPartsTableView.getSelectionModel().getSelectedItem();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initModality(Modality.NONE);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Confirm Part Deletion!");
        alert.setContentText("Are you sure you want to delete part " + part.getName() + " from parts?");
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
            System.out.println("Part deleted.");
            addedParts.remove(part);
        } else {
            System.out.println("Canceled part deletion.");
        }
    }

    /**
     * Ask user for confirmation before canceling product addition
     * and switching scene to Main Screen
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void handleCancelAddProduct(ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initModality(Modality.NONE);
        alert.setTitle("Confirmation Needed");
        alert.setHeaderText("Confirm Cancelation");
        alert.setContentText("Are you sure you want to cancel adding product?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            System.out.println("Ok selected. Product addition canceled.");
            displayScene(event, "/fxml/MainScreen.fxml");
        } else {
            System.out.println("Cancel clicked.");
        }
    }

    /**
     * Add selected part from top table view to bottom table view in order to create
     * new product
     *
     * @param event
     */
    @FXML
    void handleAddPartProduct(ActionEvent event) {
        Part part = unaddedPartsTableView.getSelectionModel().getSelectedItem();
        addedParts.add(part);
        updateAddedPartsTableView();
    }

    /**
     * Validate given product parameters.  If valid, add product to inventory,
     * else give user an error message explaining why the product is invalid.
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void handleAddProduct(ActionEvent event) throws IOException {
        String name = nameTxt.getText();
        String price = priceTxt.getText();
        String inStock = inventoryTxt.getText();
        String min = minTxt.getText();
        String max = maxTxt.getText();
        errorMessage = "";

        try {
            String errorMessage = service.addProduct(name, Double.parseDouble(price), Integer.parseInt(inStock), Integer.parseInt(min), Integer.parseInt(max), addedParts);
            if (errorMessage.length() > 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error Adding Part!");
                alert.setHeaderText("Error!");
                alert.setContentText(errorMessage);
                alert.showAndWait();
            } else {
                displayScene(event, "/fxml/MainScreen.fxml");
            }
        } catch (NumberFormatException e) {
            System.out.println("Form contains blank field.");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error Adding Product!");
            alert.setHeaderText("Error!");
            alert.setContentText("Form contains blank field.");
            alert.showAndWait();
        }

    }

    /**
     * Gets search text and inputs into lookupAssociatedPart method to highlight desired part
     *
     * @param event
     */
    @FXML
    void handleSearchPart(ActionEvent event) {
        String x = productSearchTxt.getText();
        unaddedPartsTableView.getSelectionModel().select(service.lookupPart(x));
    }


}
