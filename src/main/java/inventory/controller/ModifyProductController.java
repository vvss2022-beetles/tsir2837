package inventory.controller;

import inventory.model.Part;
import inventory.model.Product;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import static inventory.controller.MainScreenController.getModifyProductIndex;


public class ModifyProductController implements Initializable, Controller {

    // Declare fields
    private Stage stage;
    private Parent scene;
    private ObservableList<Part> addedParts = FXCollections.observableArrayList();
    private String errorMessage = new String();
    private int productIndex = getModifyProductIndex();

    private InventoryService service;

    @FXML
    private TextField minTxt;

    @FXML
    private TextField maxTxt;

    @FXML
    private TextField productIdTxt;

    @FXML
    private TextField nameTxt;

    @FXML
    private TextField inventoryTxt;

    @FXML
    private TextField priceTxt;

    @FXML
    private TextField productSearchTxt;

    @FXML
    private TableView<Part> unaddedPartsTableView;

    @FXML
    private TableColumn<Part, Integer> unaddedPartsIdCol;

    @FXML
    private TableColumn<Part, String> unaddedPartsNameCol;

    @FXML
    private TableColumn<Part, Integer> unaddedPartsInventoryCol;

    @FXML
    private TableColumn<Part, Double> unaddedPartsPriceCol;

    @FXML
    private TableView<Part> addedPartsTableView;

    @FXML
    private TableColumn<Part, Integer> addedPartsIdCol;

    @FXML
    private TableColumn<Part, String> addedPartsNameCol;

    @FXML
    private TableColumn<Part, Integer> addedPartsInventoryCol;

    @FXML
    private TableColumn<Part, Double> addedPartsPriceCol;

    public ModifyProductController() {
    }

    public void setService(InventoryService service) {
        this.service = service;
        fillWithData();
    }

    private void fillWithData() {
        // Populate add product table view
        unaddedPartsTableView.setItems(service.getAllParts());

        unaddedPartsIdCol.setCellValueFactory(new PropertyValueFactory<>("partId"));
        unaddedPartsNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        unaddedPartsInventoryCol.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        unaddedPartsPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));

        // Populate modify product form
        Product product = service.getAllProducts().get(productIndex);

        productIdTxt.setText(Integer.toString(product.getProductId()));
        nameTxt.setText(product.getName());
        inventoryTxt.setText(Integer.toString(product.getInStock()));
        priceTxt.setText(Double.toString(product.getPrice()));
        maxTxt.setText(Integer.toString(product.getMax()));
        minTxt.setText(Integer.toString(product.getMin()));

        // Populate delete product table view
        addedParts = product.getAssociatedParts();
        updateAddedPartsTableView();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }


    /**
     * Method to add to button handler to switch to scene passed as source
     *
     * @param event
     * @param source
     * @throws IOException
     */
    @FXML
    private void displayScene(ActionEvent event, String source) throws IOException {
        stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(source));
        //scene = FXMLLoader.load(getClass().getResource(source));
        scene = loader.load();
        Controller ctrl = loader.getController();
        ctrl.setService(service);
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * Method to add values of addParts to the bottom table view of the scene.
     */
    public void updateAddedPartsTableView() {
        addedPartsTableView.setItems(addedParts);

        addedPartsIdCol.setCellValueFactory(new PropertyValueFactory<>("partId"));
        addedPartsNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        addedPartsInventoryCol.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        addedPartsPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
    }

    /**
     * Ask user for confirmation before deleting selected part from current product.
     *
     * @param event
     */
    @FXML
    void handleDeletePartProduct(ActionEvent event) {
        Part part = addedPartsTableView.getSelectionModel().getSelectedItem();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initModality(Modality.NONE);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Confirm Part Deletion!");
        alert.setContentText("Are you sure you want to delete part " + part.getName() + " from parts?");
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
            System.out.println("Part deleted.");
            addedParts.remove(part);
        } else {
            System.out.println("Canceled part deletion.");
        }
    }

    /**
     * Add selected part from top table view to bottom table view in order to create
     * new product
     *
     * @param event
     */
    @FXML
    void handleAddPartProduct(ActionEvent event) {
        Part part = unaddedPartsTableView.getSelectionModel().getSelectedItem();
        addedParts.add(part);
        updateAddedPartsTableView();
    }

    /**
     * Ask user for confirmation before canceling product modification
     * and switching scene to Main Screen
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void handleCancelModifyProduct(ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initModality(Modality.NONE);
        alert.setTitle("Confirmation Needed");
        alert.setHeaderText("Confirm Cancelation");
        alert.setContentText("Are you sure you want to cancel modifying product?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            System.out.println("Ok selected. Product modification canceled.");
            displayScene(event, "/fxml/MainScreen.fxml");
        } else {
            System.out.println("Cancel clicked.");
        }
    }

    /**
     * Validate given product parameters.  If valid, update product in inventory,
     * else give user an error message explaining why the product is invalid.
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void handleModifyProduct(ActionEvent event) throws IOException {
        String productId = productIdTxt.getText();
        String name = nameTxt.getText();
        String price = priceTxt.getText();
        String inStock = inventoryTxt.getText();
        String min = minTxt.getText();
        String max = maxTxt.getText();
        errorMessage = "";

        try {
            errorMessage = Product.isValidProduct(name, Double.parseDouble(price), Integer.parseInt(inStock), Integer.parseInt(min), Integer.parseInt(max), addedParts, errorMessage);
            if (errorMessage.length() > 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error Adding Part!");
                alert.setHeaderText("Error!");
                alert.setContentText(errorMessage);
                alert.showAndWait();
            } else {
                service.updateProduct(productIndex, new Product(Integer.parseInt(productId), name, Double.parseDouble(price), Integer.parseInt(inStock), Integer.parseInt(min), Integer.parseInt(max), addedParts));
                displayScene(event, "/fxml/MainScreen.fxml");
            }
        } catch (NumberFormatException e) {
            System.out.println("Form contains blank field.");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error Adding Product!");
            alert.setHeaderText("Error!");
            alert.setContentText("Form contains blank field.");
            alert.showAndWait();
        }
    }

    /**
     * Gets search text and inputs into lookupAssociatedPart method to highlight desired part
     *
     * @param event
     */
    @FXML
    void handleSearchPart(ActionEvent event) {
        String x = productSearchTxt.getText();
        unaddedPartsTableView.getSelectionModel().select(service.lookupPart(x));
    }

}
